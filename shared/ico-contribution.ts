export type ContributionCurrency = 'BTC' | 'ETH' | 'LTC';

export interface IcoContribution {
  address: string;
  currency: ContributionCurrency;

  /**
   * In real application this field should not be stored as number, but rather
   * as string. Because the value can go beyond Number.MAX_SAFE_INTEGER since
   * 1 ether is 10^18 wei.
   */
  value: number;
  txid: string;
  date: string;
}
