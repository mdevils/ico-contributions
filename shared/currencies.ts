import {ContributionCurrency} from './ico-contribution';

export const baseCurrency: ContributionCurrency = 'BTC';
export const currencySubUnitRates: {[key in ContributionCurrency]: number} = {
  // Goes beyond Number.MAX_SAFE_INTEGER, should use bignumbers library.
  ETH: Math.pow(10, 18),
  BTC: Math.pow(10, 8),
  LTC: Math.pow(10, 6)
};
export const currencies: ContributionCurrency[] = ['BTC', 'ETH', 'LTC'];
