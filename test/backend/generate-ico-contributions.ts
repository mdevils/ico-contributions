import {expect} from 'chai';
import {
  coins,
  generateIcoContributions
} from '../../backend/generate-ico-contributions';
import {ContributionCurrency} from '../../shared/ico-contribution';

const generateIterations = 5000;

const validatorsPerCurrency: {[key in ContributionCurrency]: {
  addressRegExp: RegExp,
  transactionIdRegExp: RegExp
}} = {
  BTC: {
    addressRegExp: /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/,
    transactionIdRegExp: /^[0-9a-f]{64}$/
  },
  ETH: {
    addressRegExp: /^0x[0-9a-f]{40}$/,
    transactionIdRegExp: /^0x[0-9a-f]{64}$/
  },
  LTC: {
    addressRegExp: /^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/,
    transactionIdRegExp: /^[0-9a-f]{64}$/
  }
};

// Tests for generators are a bit weird because it relies on a random factor.

describe('generate-ico-contributions', () => {
  const currencies = (
    Object.keys(validatorsPerCurrency) as ContributionCurrency[]
  );

  for (const currency of currencies) {
    const validators = validatorsPerCurrency[currency];
    const coin = coins.find((currentCoin) => currentCoin.currency === currency);
    describe(currency, () => {
      describe('generateAddress()', () => {
        it('should generate a valid address', () => {
          for (let i = 0; i < generateIterations; i++) {
            expect(coin.generateAddress()).to.match(validators.addressRegExp);
          }
        });
      });
      describe('generateTransactionId()', () => {
        it('should generate a valid transaction id', () => {
          for (let i = 0; i < generateIterations; i++) {
            expect(coin.generateTransactionId())
              .to.match(validators.transactionIdRegExp);
          }
        });
      });
    });
  }

  describe('generateIcoContributions()', () => {
    it('should generate exact number of documents', () => {
      expect(generateIcoContributions(0)).to.have.lengthOf(0);
      expect(generateIcoContributions(1)).to.have.lengthOf(1);
      expect(generateIcoContributions(10)).to.have.lengthOf(10);
      expect(generateIcoContributions(100)).to.have.lengthOf(100);
    });

    it('should have integer value', () => {
      for (const contribution of generateIcoContributions(generateIterations)) {
        expect(contribution.value).to.equal(Math.floor(contribution.value));
      }
    });

    it('should generate valid addresses and transaction ids', () => {
      for (const contribution of generateIcoContributions(generateIterations)) {
        const validators = validatorsPerCurrency[contribution.currency];
        expect(contribution.address).to.match(validators.addressRegExp);
        expect(contribution.txid).to.match(validators.transactionIdRegExp);
      }
    });

    it('should provide valid currency', () => {
      for (const contribution of generateIcoContributions(generateIterations)) {
        expect(contribution.currency).to.be.oneOf(currencies);
      }
    });

    it('should have a numeric value', () => {
      for (const contribution of generateIcoContributions(generateIterations)) {
        expect(contribution.value).to.be.a('number');
      }
    });
  });
});
