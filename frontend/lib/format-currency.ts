import {currencySubUnitRates} from '../../shared/currencies';

const formatter = new Intl.NumberFormat('en-US', {
  useGrouping: false,
  maximumFractionDigits: 8
});

export function formatCurrency(
  valueInSubUnit: number,
  currency: string
): string {
  const formattedValue = formatter.format(
    valueInSubUnit / currencySubUnitRates[currency]
  );
  return `${currency}${formattedValue}`;
}
