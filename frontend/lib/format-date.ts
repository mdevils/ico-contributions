const formatter = new Intl.DateTimeFormat('de-DE');

export function formatDate(date: Date) {
  return formatter.format(date);
}
