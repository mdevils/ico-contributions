import {baseCurrency, currencies} from '../../shared/currencies';
import {ContributionCurrency} from '../../shared/ico-contribution';
import {CurrencyRates} from '../types/currency-rates';
import {createAsync} from './create-action';

async function loadRate(
  base: ContributionCurrency,
  target: ContributionCurrency
): Promise<{
  base: ContributionCurrency,
  target: ContributionCurrency,
  value: number
}> {
  const response = await fetch(
    `https://api.cryptonator.com/api/full/${base}-${target}`
  );
  const data: {ticker: {price: string}} = await response.json();
  return {
    base,
    target,
    value: Number(data.ticker.price)
  };
}

export const loadCurrencyRates = createAsync(
  'LOAD_CURRENCY_RATES',
  async ({}: {}) => {
    const currenciesToRequest = currencies
      .filter((currency) => currency !== baseCurrency);
    const rates = await Promise.all(currenciesToRequest.map((currency) => {
      return loadRate(currency, baseCurrency);
    }));
    return rates.reduce((result, rate) => {
      result[rate.base] = rate.value;
      return result;
    }, {[baseCurrency]: 1}) as CurrencyRates;
  }
);
