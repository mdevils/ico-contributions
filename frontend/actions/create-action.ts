import actionCreatorFactory from 'typescript-fsa';
import {asyncFactory} from 'typescript-fsa-redux-thunk';
import {State} from '../types/state';

export const createAction = actionCreatorFactory();
export const createAsync = asyncFactory<State>(createAction);
