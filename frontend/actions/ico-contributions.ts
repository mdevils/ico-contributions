import {request} from 'graphql-request';
import {IcoContribution} from '../../shared/ico-contribution';
import {backendApiUrl} from '../config';
import {createAction, createAsync} from './create-action';

export const loadIcoContributions = createAsync(
  'LOAD_ICO_CONTRIBUTIONS',
  async ({}: {}) => {
    return ((await request(
      backendApiUrl,
      `{
        icoContributions {
          address
          currency
          value
          txid
          date
        }
      }`
    )) as any).icoContributions as IcoContribution[];
  }
);

export type IcoContributionsGroupCriteria = 'date' | 'currency' | 'amount';

export const allIcoContributionsGroupCriterias: (
  IcoContributionsGroupCriteria[]
) = ['date', 'currency', 'amount'];

export const groupIcoContributions = createAction<{
  criteria: IcoContributionsGroupCriteria;
  enabled: boolean;
}>('GROUP_ICO_CONTRIBUTIONS');

export const showIcoContributionsWhenGrouped = createAction<{
  show: boolean;
}>('SHOW_ICO_CONTRIBUTIONS_WHEN_GROUPED');
