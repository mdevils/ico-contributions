import * as React from 'react';
import {connect} from 'react-redux';
import {IcoContribution} from '../../../shared/ico-contribution';
import {
  groupIcoContributions,
  showIcoContributionsWhenGrouped
} from '../../actions/ico-contributions';
import {
  areIcoContributionsLoading,
  ContributionsGroupedResult,
  getGroupedIcoContributionsList,
  getIcoContributionsGrouping,
  getIcoContributionsList,
  getIcoContributionsLoadingError,
  shouldShowListWhenGrouped
} from '../../selectors/ico-contributions';
import {State} from '../../types/state';
import {ContributionsList} from '../contributions-list';
import {GroupContributionsPanel} from '../group-contributions-panel';
import {GroupedContributionsList} from '../grouped-contributions-list';
import './styles.css';

const GroupContributionsPanelConnected = connect(
  (state: State) => ({
    groupBy: getIcoContributionsGrouping(state),
    showListWhenGrouped: shouldShowListWhenGrouped(state)
  }),
  {
    groupIcoContributions,
    showIcoContributionsWhenGrouped
  }
)(GroupContributionsPanel);

export function App({
  loading,
  loadingError,
  contributions,
  groupedContributions,
  showListWhenGrouped
}: {
  loading: boolean,
  loadingError: string,
  contributions: IcoContribution[],
  groupedContributions: ContributionsGroupedResult,
  showListWhenGrouped: boolean
}) {
  let list;

  if (loading) {
    list = (<div className='app__loading'>Loading...</div>);
  }

  if (loadingError) {
    list = (<div className='app__loading-error'>{loadingError}</div>);
  }

  if (!list && groupedContributions) {
    list = (
      <GroupedContributionsList
        showListWhenGrouped={showListWhenGrouped}
        groupedContributions={groupedContributions} />
    );
  }

  if (!list) {
    list = (<ContributionsList contributions={contributions} />);
  }

  return (
    <div className='app'>
      <div className='app__title'>
        ICO Contributions Demo
      </div>
      <div className='app__header'>
        <GroupContributionsPanelConnected />
      </div>
      {list}
      <div className='app__footer'>
        Developed with pleasure at 24.2.2019 by Marat Dulin
      </div>
    </div>
  );
}

export const AppConnected = connect(
  (state: State) => ({
    loading: areIcoContributionsLoading(state),
    loadingError: getIcoContributionsLoadingError(state),
    contributions: getIcoContributionsList(state),
    groupedContributions: getGroupedIcoContributionsList(state),
    showListWhenGrouped: shouldShowListWhenGrouped(state)
  })
)(App);
