import * as React from 'react';
import {baseCurrency} from '../../../shared/currencies';
import {formatCurrency} from '../../lib/format-currency';
import {ContributionsGroupedResult} from '../../selectors/ico-contributions';
import {ContributionsList} from '../contributions-list';
import {groupingTitles} from '../group-contributions-panel';

import './styles.css';

export function GroupedContributionsList({
  groupedContributions,
  showListWhenGrouped
}: {
  groupedContributions: ContributionsGroupedResult,
  showListWhenGrouped: boolean
}) {
  return (
    <div className='grouped-contributions-list'>
      {groupedContributions.groups.map((group) => {
        let children;

        if (group.subGroupedResult) {
          children = (
            <div className='grouped-contributions-list__sub-group'>
              <GroupedContributionsList
                groupedContributions={group.subGroupedResult}
                showListWhenGrouped={showListWhenGrouped} />
            </div>
          );
        }

        if (!children && showListWhenGrouped) {
          children = (
            <div className='grouped-contributions-list__contributions'>
              <ContributionsList contributions={group.contributions}/>
            </div>
          );
        }

        return (
          <div
            className='grouped-contributions-list__group'
            key={group.criteriaValue}>
            <div className='grouped-contributions-list__criteria'>
              <div className='grouped-contributions-list__criteria-title'>
                {groupingTitles[groupedContributions.criteria]}:
              </div>
              <div className='grouped-contributions-list__criteria-value'>
                {group.criteriaValue}
              </div>
              <div className='grouped-contributions-list__criteria-total'>
                {formatCurrency(group.totalValueInBaseCurrency, baseCurrency)}
              </div>
            </div>
            {children}
          </div>
        );
      })}
    </div>
  );
}
