import * as React from 'react';
import {
  allIcoContributionsGroupCriterias,
  groupIcoContributions as groupIcoContributionsAction,
  IcoContributionsGroupCriteria,
  showIcoContributionsWhenGrouped as showIcoContributionsWhenGroupedAction
} from '../../actions/ico-contributions';
import './styles.css';

export const groupingTitles: (
  {[key in IcoContributionsGroupCriteria]: string}
) = {
  currency: 'Currency',
  amount: 'Amount',
  date: 'Date'
};

export function GroupContributionsPanel({
  groupBy,
  showListWhenGrouped,
  groupIcoContributions,
  showIcoContributionsWhenGrouped
}: {
  groupBy: IcoContributionsGroupCriteria[],
  showListWhenGrouped: boolean,
  groupIcoContributions: typeof groupIcoContributionsAction,
  showIcoContributionsWhenGrouped: typeof showIcoContributionsWhenGroupedAction
}) {
  const activeGrouping = groupBy;
  // Due to a very small amount of items we can keep this ineffective method.
  const inactiveGrouping = allIcoContributionsGroupCriterias.filter(
    (criteria) => groupBy.indexOf(criteria) === -1
  );
  const groupings = activeGrouping.map((criteria) => ({
    criteria,
    enabled: true
  })).concat(inactiveGrouping.map((criteria) => ({
    criteria,
    enabled: false
  })));
  return (
    <div className='group-contributions-panel'>
      {groupings.map(({criteria, enabled}) => {
        let className = 'group-contributions-panel__group';
        if (enabled) {
          className += ' group-contributions-panel__group_active';
        }
        return (
          <label className={className} key={criteria}>
            <input
              type='checkbox'
              checked={enabled}
              onChange={() => {
                groupIcoContributions({
                  criteria,
                  enabled: !enabled
                });
              }}
            />
            <div className='group-contributions-panel__group-title'>
              {groupingTitles[criteria]}
            </div>
          </label>
        );
      })}
      <label className='group-contributions-panel__show-list-when-grouped'>
        <input
          type='checkbox'
          checked={showListWhenGrouped}
          onChange={() => showIcoContributionsWhenGrouped({
            show: !showListWhenGrouped
          })}
        />
        <div
          className='group-contributions-panel__show-list-when-grouped-title'>
          Show contribution list when grouped
        </div>
      </label>
    </div>
  );
}
