import * as React from 'react';
import {IcoContribution} from '../../../shared/ico-contribution';
import {formatCurrency} from '../../lib/format-currency';
import {formatDate} from '../../lib/format-date';
import './styles.css';

export function ContributionsList({contributions}: {
  contributions: IcoContribution[]
}) {
  return (
    <div className='contribution-list'>
      {contributions.map((contribution) => (
        <div
          className='contribution-list__item'
          key={`${contribution.currency}/${contribution.txid}`}>
          <div className='contribution-list__props'>
            <div className='contribution-list__prop'>
              <div className='contribution-list__prop-label'>ADDR</div>
              <div className='contribution-list__prop-content'>
                {contribution.address}
              </div>
            </div>
            <div className='contribution-list__prop'>
              <div className='contribution-list__prop-label'>TXID</div>
              <div className='contribution-list__prop-content'>
                {contribution.txid}
              </div>
            </div>
            <div className='contribution-list__prop'>
              <div className='contribution-list__prop-label'>DATE</div>
              <div className='contribution-list__prop-content'>
                {/* can be optimized */}
                {formatDate(new Date(contribution.date))}
              </div>
            </div>
          </div>

          <div className='contribution-list__value'>
            {formatCurrency(contribution.value, contribution.currency)}
          </div>
        </div>
      ))}
    </div>
  );
}
