import {reducerWithInitialState} from 'typescript-fsa-reducers';
import {IcoContribution} from '../../shared/ico-contribution';
import {
  groupIcoContributions,
  IcoContributionsGroupCriteria,
  loadIcoContributions, showIcoContributionsWhenGrouped
} from '../actions/ico-contributions';

const initialState = {
  areLoading: true,
  loadingError: null as null | string,
  list: [] as IcoContribution[],
  groupBy: [] as IcoContributionsGroupCriteria[],
  showListWhenGrouped: true
};

export type IcoContributionsStoreState = typeof initialState;

export const icoContributionsReducer = reducerWithInitialState(initialState)
  .case(loadIcoContributions.async.started, (state) => {
    return {
      ...state,
      areLoading: true,
      loadingError: null,
      list: []
    };
  })
  .case(loadIcoContributions.async.failed, (state, {error}) => {
    return {
      ...state,
      areLoading: false,
      loadingError: error.message,
      list: []
    };
  })
  .case(loadIcoContributions.async.done, (state, {result}) => {
    return {
      ...state,
      areLoading: false,
      loadingError: null,
      list: result
    };
  })
  .case(groupIcoContributions, (state, {criteria, enabled}) => {
    const groupBy = enabled ?
      state.groupBy.concat(criteria) :
      state.groupBy.filter((currentCriteria) => currentCriteria !== criteria);
    return {
      ...state,
      groupBy
    };
  })
  .case(showIcoContributionsWhenGrouped, (state, {show}) => {
    return {
      ...state,
      showListWhenGrouped: show
    };
  });
