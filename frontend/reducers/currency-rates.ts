import {reducerWithInitialState} from 'typescript-fsa-reducers';
import {loadCurrencyRates} from '../actions/currency-rates';
import {CurrencyRates} from '../types/currency-rates';

export const initialState = {
  rates: null as null | CurrencyRates
};

export type CurrencyRatesStoreState = typeof initialState;

export const currencyRatesReducer = reducerWithInitialState(initialState)
  .case(loadCurrencyRates.async.done, (state, {result}) => {
    return {
      ...state,
      rates: result
    };
  });
