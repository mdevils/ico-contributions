import {CurrencyRatesStoreState} from '../reducers/currency-rates';
import {IcoContributionsStoreState} from '../reducers/ico-contributions';

export interface State {
  icoContributions: IcoContributionsStoreState;
  currencyRates: CurrencyRatesStoreState;
}
