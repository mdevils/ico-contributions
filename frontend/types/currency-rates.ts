import {ContributionCurrency} from '../../shared/ico-contribution';

/**
 * Currency rates are stored in base currency.
 */
export type CurrencyRates = {
  [key in ContributionCurrency]: number;
};
