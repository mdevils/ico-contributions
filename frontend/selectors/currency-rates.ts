import {State} from '../types/state';

export function getCurrencyRates(state: State) {
  return state.currencyRates.rates;
}
