import * as R from 'ramda';
import {createSelector} from 'reselect';
import {baseCurrency, currencySubUnitRates} from '../../shared/currencies';
import {IcoContribution} from '../../shared/ico-contribution';
import {IcoContributionsGroupCriteria} from '../actions/ico-contributions';
import {formatDate} from '../lib/format-date';
import {CurrencyRates} from '../types/currency-rates';
import {State} from '../types/state';
import {getCurrencyRates} from './currency-rates';

export function getIcoContributionsList(state: State) {
  return state.icoContributions.list;
}

export function getIcoContributionsLoadingError(state: State) {
  return state.icoContributions.loadingError;
}

export function areIcoContributionsLoading(state: State) {
  return state.icoContributions.areLoading;
}

export function getIcoContributionsGrouping(state: State) {
  return state.icoContributions.groupBy;
}

export function shouldShowListWhenGrouped(state: State) {
  return state.icoContributions.showListWhenGrouped;
}

export interface ContributionsGroup {
  criteriaValue: string;
  totalValueInBaseCurrency: number;
  subGroupedResult: ContributionsGroupedResult | null;
  contributions: IcoContribution[] | null;
}

export interface ContributionsGroupedResult {
  criteria: IcoContributionsGroupCriteria;
  groups: ContributionsGroup[];
}

export function getContributionBaseValue(
  contribution: IcoContribution,
  currencyRates: CurrencyRates
) {
  return (
    (
      contribution.value / currencySubUnitRates[contribution.currency]
    ) *
    currencyRates[contribution.currency] *
    currencySubUnitRates[baseCurrency]
  );
}

function createGroup({
  criteriaValue,
  contributions,
  currencyRates,
  criterias
}: {
  criteriaValue: string,
  contributions: IcoContribution[],
  currencyRates: CurrencyRates,
  criterias: IcoContributionsGroupCriteria[]
}): ContributionsGroup {
  return {
    criteriaValue,
    contributions,
    subGroupedResult: criterias.length > 0 ?
      groupByCriteria(criterias, contributions, currencyRates) :
      null,
    totalValueInBaseCurrency: contributions.reduce((result, contribution) => {
      return result + getContributionBaseValue(contribution, currencyRates);
    }, 0)
  };
}

function groupByCriteria(
  criterias: IcoContributionsGroupCriteria[],
  contributions: IcoContribution[],
  currencyRates: CurrencyRates
): ContributionsGroupedResult {
  const criteria = criterias[0];
  let groups: {[key: string]: IcoContribution[]};

  if (criteria === 'date') {
    groups = R.groupBy(
      // can be optimized
      (({date}) => formatDate(new Date(date))),
      contributions
    );
  }

  if (criteria === 'currency') {
    groups = R.groupBy(
      ({currency}) => currency,
      contributions
    );
  }

  if (criteria === 'amount') {
    // Can be optimized to calculate in one run.
    const min = contributions.reduce((result, contribution) => {
      return Math.min(
        result,
        getContributionBaseValue(contribution, currencyRates)
      );
    }, Infinity);
    const max = contributions.reduce((result, contribution) => {
      return Math.max(
        result,
        getContributionBaseValue(contribution, currencyRates)
      );
    }, -Infinity);

    const top10Percent = min + (max - min) * 0.9;
    const bottom10Percent = min + (max - min) * 0.1;
    groups = R.groupBy(
      (contribution) => {
        const valueInBaseCurrency = (
          getContributionBaseValue(contribution, currencyRates)
        );
        if (valueInBaseCurrency < bottom10Percent) {
          return 'Bottom 10%';
        }
        if (valueInBaseCurrency > top10Percent) {
          return 'Top 10%';
        }
        return 'The rest';
      },
      contributions
    );
  }

  return {
    criteria,
    groups: Object.keys(groups).map((criteriaValue) => (
      createGroup({
        criteriaValue,
        contributions: groups[criteriaValue],
        currencyRates,
        criterias: criterias.slice(1)
      })
    ))
  };
}

export const getGroupedIcoContributionsList = createSelector(
  [
    getIcoContributionsList,
    getIcoContributionsGrouping,
    getCurrencyRates
  ],
  (
    icoContributions,
    groupBy,
    currencyRates
  ): ContributionsGroupedResult | null => {
    if (!currencyRates) {
      return null;
    }
    if (icoContributions.length === 0) {
      return null;
    }
    if (groupBy.length === 0) {
      return null;
    }
    return groupByCriteria(groupBy, icoContributions, currencyRates);
  }
);
