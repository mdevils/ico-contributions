import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {loadCurrencyRates} from './actions/currency-rates';
import {loadIcoContributions} from './actions/ico-contributions';
import {AppConnected} from './components/app';
import {currencyRatesReducer} from './reducers/currency-rates';
import {icoContributionsReducer} from './reducers/ico-contributions';

const composeEnhancers = (
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
);

const store = createStore(
  combineReducers({
    icoContributions: icoContributionsReducer,
    currencyRates: currencyRatesReducer
  }),
  composeEnhancers(applyMiddleware(thunk))
);

store.dispatch(loadIcoContributions.action() as any);
store.dispatch(loadCurrencyRates.action() as any);

ReactDOM.render(
  <Provider store={store}>
    <AppConnected />
  </Provider>,
  document.getElementById('root')
);
