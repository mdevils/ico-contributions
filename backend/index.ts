import {ApolloServer} from 'apollo-server';
import * as fs from 'fs';
import * as path from 'path';
import {generateIcoContributions} from './generate-ico-contributions';

const typeDefs = fs.readFileSync(
  path.join(__dirname, 'schema.graphqls'),
  'utf8'
);

const resolvers = {
  Query: {
    icoContributions: () => generateIcoContributions(100)
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers
});

server.listen(4000).then(({url}) => {
  // tslint:disable-next-line:no-console
  console.log(`🚀  Server ready at ${url}`);
});
