import {
  ContributionCurrency,
  IcoContribution
} from '../shared/ico-contribution';

interface CoinDefinition {
  currency: ContributionCurrency;
  generateAddress(): string;
  generateTransactionId(): string;
  generateValue(): number;
}

function getRandomInRange(min: number, max: number) {
  return min + Math.round((max - min) * Math.random());
}

const bitcoinStartingSymbols = ['1', '3'];
const ltcStartingSymbols = ['L', 'M', '3'];

const validBitcoinSymbols = ([
  // 0..9
  ...Array.from({length: 9}, ({}: any, i: number) => String(i + 1)),
  // a..z
  ...Array.from({length: 26}, ({}: any, i: number) => String.fromCharCode(
    97 + i
  )),
  // A..Z
  ...Array.from({length: 26}, ({}: any, i: number) => String.fromCharCode(
    65 + i
  ))
]).filter((char) => {
  // Excluding 0, O, l, I. These chars are invalid to prevent visual ambiguity.
  return char !== '0' && char !== 'O' && char !== 'l' && char !== 'I';
});

function generateRandomHexSymbol() {
  return Math.round(Math.random() * 15).toString(16);
}

function pickRandomItemFromArray<T>(array: T[]): T {
  return array[
    Math.round(Math.random() * (array.length - 1))
  ];
}

function generateRandomBitcoinAddressSymbol() {
  return pickRandomItemFromArray(validBitcoinSymbols);
}

export const coins: CoinDefinition[] = [
  {
    currency: 'BTC',
    /**
     * Bitcoin address starts with 1 or 3 and then 25-34 alphanumeric
     * characters with an exception for 0, O, l, I.
     */
    generateAddress() {
      return (
        pickRandomItemFromArray(bitcoinStartingSymbols) +
        Array.from(
          {length: getRandomInRange(25, 34)},
          generateRandomBitcoinAddressSymbol
        ).join('')
      );
    },
    /**
     * Transaction ID is 64 hexadecimal characters.
     */
    generateTransactionId() {
      return (
        Array.from({length: 64}, generateRandomHexSymbol).join('')
      );
    },
    generateValue() {
      return getRandomInRange(10000, 100000000);
    }
  },
  {
    currency: 'ETH',
    generateAddress() {
      return (
        `0x${Array.from({length: 40}, generateRandomHexSymbol).join('')}`
      );
    },
    generateTransactionId() {
      return (
        `0x${Array.from({length: 64}, generateRandomHexSymbol).join('')}`
      );
    },
    generateValue() {
      return getRandomInRange(10000000000000, 10000000000000000);
    }
  },
  {
    currency: 'LTC',
    /**
     * LTC address starts with L or M or 3 and then 26-33 alphanumeric
     * characters with an exception for 0, O, l, I.
     */
    generateAddress() {
      return (
        pickRandomItemFromArray(ltcStartingSymbols) +
        Array.from(
          {length: getRandomInRange(26, 33)},
          generateRandomBitcoinAddressSymbol
        ).join('')
      );
    },
    /**
     * Transaction ID is 64 hexadecimal characters.
     */
    generateTransactionId() {
      return (
        Array.from({length: 64}, generateRandomHexSymbol).join('')
      );
    },
    generateValue() {
      return getRandomInRange(100000, 100000000);
    }
  }
];

export function generateIcoContributions(count: number): IcoContribution[] {
  return Array.from(
    {length: count},
    () => {
      const coin = pickRandomItemFromArray(coins);
      return {
        address: coin.generateAddress(),
        currency: coin.currency,
        value: coin.generateValue(),
        txid: coin.generateTransactionId(),
        date: new Date(Date.now() - getRandomInRange(
          0,
          1000 * 3600 * 24 * 30 // 30 last days
        )).toISOString()
      };
    }
  );
}
