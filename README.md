# ICO Contribution List

A simple demo application written in TypeScript using GraphQL,
React and Redux.

Features:

* List ICO contribution list.
* Group ICO contributions by currency, amount, date.
* Grouping can be applied in different order.

## Running

* Clone the repository.
* Install dependencies:
```
yarn install
```
* Run backend:
```
yarn backend
```
* Run frontend:
```
yarn frontend
```
* Open http://localhost:8080/

## Testing

You can run unit tests and lint the code using the following command:
```
yarn test
```

## Notes

1. For currencies `number` type was chosen for simplicity.
   In a proper financial website bignumber/currency libraries should be
   used instead.
2. Reselect was used for caching purposes (to cache grouping results).
3. Some of the functions were not optimized and an explicit comment
   can be found there.
4. Even though I used redux for state management, it's not actually
   required since GraphQL is used and can be replaced by Apollo Client.
5. Some libraries were included for just one function, i.e. Ramda.
   But including ramda is a good thing in general and will be useful
   in the future development of the project.
6. Intl was used for number and date formatting (which is good).
   In production it might be required to add a polyfill depending on the
   desired browser support.
7. I could not find a proper library to format cryptocurrencies.
8. Cryptocurrencies conversion rates are requested via open API.
9. I tried to keep everything more or less simple :)
